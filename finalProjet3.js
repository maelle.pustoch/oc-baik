let selectedStation;
let emptySignature = true;
let counterPixels = 0;

document.getElementById("button-submit").disabled = true;

function prefillForm() {
  let getNameFill = localStorage.getItem('name');
  document.getElementById('name').value = getNameFill;
  let getFamilyNameFill = localStorage.getItem('familyname');
  document.getElementById('family-name').value = getFamilyNameFill;
} prefillForm();


let jumbotronPassMobile = document.getElementById('subscription')
function hidejumbotronMobile () {
    jumbotronPassMobile.style.display = "none"; 
};

let newsMobile = document.getElementById('news')
function hidenewsMobile () {
    newsMobile.style.display = "none"; 
};


// Set effects sliding for interface
$(document).ready(function() {
  $("#menumapid").click(function() { // Gestion du Scroll
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#mapid").offset().top
    }, 2000);
	});

 	$("#menunews").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#news").offset().top
    }, 2000);
  });
  
  $("#menufooter").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#footer").offset().top
    }, 2000);
  });

  $("#menusubscription").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#subscription").offset().top
    }, 2000);
  });

  $( "#button-subscriptionshow" ).click(function() {
	  $( "#subscription" ).slideToggle( "slow" );
	});
  
  $( "#button-newsshow" ).click(function() {
	  $( "#news" ).slideToggle( "slow" );
	});

  function pressKey (e) {
    if(e.keyCode === 37) { 
    	$( ".prev" ).trigger( "click" );
    } else if(e.keyCode === 39) {
    	$( ".next" ).trigger( "click" );
    	e.stopImmediatePropagation();
  	}
  }
	document.addEventListener('keydown', pressKey);

  $( ".btn-primary-formshow" ).click(function() { // Gestion du Form
  	$( ".showform" ).slideToggle( "slow" );
  });
});


function Station(address, bikeStands, bikes) {
  this.address = address;
  this.bikeStands = bikeStands;
  this.bikes = bikes;
	this.stationSummary = function() {
	};
}

function clearBooking() {
  localStorage.clear();
  sessionStorage.clear();
}

function clearEverything() {
  clearBooking();
	console.clear();
	clearSignature();
	resetFields();
	hideStationSubmission();
}

function resetFields() {
  let resetForm = document.querySelectorAll('div.form-group input');
  resetForm.forEach(function (input){
    input.value = "";
    document.getElementById('exampleCheck1').checked = false;
  });
}

function Customer(name, familyName) {
  this.name = name //prend la valeur du champs Name
  this.familyname = familyName; // retourne la valeur de family name
}

function Booking(client, fromStation) { //de quoi dépend une reservation
  // Les propriétés d'une réservation : quels sont les "traits" qui la caractérisent ?
  this.startTime = null; // Un temps de début - un panier tant que que resa non validée
  this.endTime = null; // Un temps de fin
  this.customer = client;  // Un client oarce que customer = argument
  this.fromStation = fromStation; // de quelle station prend on la resa
  this.limitTime = 1200000;

  this.start = function() { //methode qui a impact sur la startTime 
  	this.startTime = new Date().getTime();
  	this.endTime = this.startTime + this.limitTime;
  	this.storeBooking();
  }

  this.storeBooking = function() {

  	localStorage.setItem("familyname", this.customer.familyname);
    localStorage.setItem("name", this.customer.name);

    sessionStorage.setItem('station', this.fromStation);
    sessionStorage.setItem('endTime', this.endTime);
    sessionStorage.setItem('startTime', this.startTime);
    console.log('Stored booking')
  }
}

function Slider(sliderElement, timeOut) {
	this.sliderElement = sliderElement;
	this.timeOut = timeOut;
	this.blocks = sliderElement.querySelectorAll('.carrousel-wrapper');
	this.currentBlock = null;
	this.i = 0;
	this.iMax = this.blocks.length - 1;
	this.timer = null;

	this.init = function() {
		
		let that = this;
		this.update();

		let nextButton = sliderElement.querySelector('.next');
		//console.log(nextButton)
		if(nextButton != undefined) {
			nextButton.addEventListener('click', function() {
				that.next();
				that.update();
				// //console.log('Le bouton Prev a été pause a été appuyé');
			})
		}

		let prevButton = sliderElement.querySelector('.prev');
		if(prevButton != undefined) {
			prevButton.addEventListener('click', function() {
				that.prev();
				that.update();
				//console.log('Le bouton Next a été pause a été appuyé');
			})
		}
		
		let pauseButton = sliderElement.querySelector('.pause');
		if(pauseButton != undefined) {
			pauseButton.addEventListener('click', function() {
				that.pause();
				that.update();
				//console.log('Le bouton Pause a été pause a été appuyé');
			})
		}

		let playButton = sliderElement.querySelector('.play');
		if(playButton != undefined) {
			playButton.addEventListener('click', function() {
				that.start();
				// that.update();
				//console.log('Le bouton Play a été appuyé');
			})
		}
	}

	this.update = function() { //calcule le currentBlock a affiche
		this.currentBlock = this.blocks[this.i]; //this.currentImg[0]
		this.blocks.forEach(function(item) { //je recupere la valeur
			item.style.display = "none";
		});
		this.currentBlock.style.display = "block";
	}
	
	this.start = function() {
		let that = this; // quand
		this.timer = setTimeout(function() {
			if(that.i < that.iMax) {
				that.i++;
			} else {
				that.i = 0;
			} 
			that.update();
			that.start();
		}, this.timeOut);
		//console.log("l'image a changé");
	}

	this.next = function() {
		//console.log('next()');
		if(this.i < this.iMax) {
			this.i++;
		} else {
			this.i = 0;
		}
		this.pause();
		
	}

	this.prev = function() { //*attention inversement
		//console.log('prev()')
		if(this.i == 0) {
			this.i = this.iMax;
		} else {
			this.i--;
		}
		this.pause();		
	}

	this.pause = function() {
		clearTimeout(this.timer);
	}

	}

let mySliderElement = document.getElementById('carrousel');
let myTimeOut = 5000;
let mySlider = new Slider(mySliderElement, myTimeOut);
mySlider.init();
mySlider.start();

// MAPS
// Latitude : 49.411402 | Longitude : 1.078647

const mymap = L.map('mapid', {
        zoomDelta: 0.25,
        zoomSnap: 0,
        minZoom: 13
    }); // declare que mymap correspond à la map et ne doit pas être modifié (const)
mymap.setView([49.443232, 1.099971], 10);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 22,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoibWFlbGxlcnMiLCJhIjoiY2pzOWFic285MHlqcDQzb2t3NjI1czl6cCJ9.hgemEc6YCpS1AeUCz8iHRQ'//My Access Token//
}).addTo(mymap);

let markers = L.markerClusterGroup();
let standardIcon = L.icon({
    iconUrl: './graphism-baik/map-marker-teal.png',
    iconSize:     [30, 30], // size of the icon
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor:  [0,0]
     // point from which the popup should open relative to the iconAnchor
});

let worksIcon = L.icon({
    iconUrl: './graphism-baik/marker-cones.png',
    iconSize:     [30, 30], // size of the icon
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor:  [0,0]
     // point from which the popup should open relative to the iconAnchor
});


let closedIcon = L.icon({
    iconUrl: './graphism-baik/marker-closed.png',
    iconSize:     [30, 30], // size of the icon
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor:  [0,0]
     // point from which the popup should open relative to the iconAnchor
});


let otherIcon = L.Icon.extend({
    options: {
        iconSize:     [30, 30], // size of the icon
        iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor:  [0,0]
         // point from which the popup should open relative to the iconAnchor
    }
});


// let redIcon = new otherIcon({iconUrl: './marker-closed.png'}),
//     orangeIcon = new otherIcon({iconUrl: './marker-cones.png'});
let marker = L.marker([49.443232, 1.099971], {icon: standardIcon}); //Coordonées GPS Rouen


//----BUTTONS & DISPLAY

let formSideBar = document.getElementById("form");
let currentStation = null; // nul car on veut remplir par la station que l'on veut visionner
let buttonLegend = document.getElementById("button-legend");
let legendSideBar = document.getElementById("legend");
let legendSideBarMobile = document.getElementById("legend-mobile");
let formDown = document.getElementById("showform");
let buttonClose = document.getElementById("close-icon");
let buttonCancel =document.getElementById("button-cancel");



 buttonCancel.addEventListener('click', function(e) {
    clearEverything();
         // au clic sur cancel, une fonction efface tout
  }); 

//--LEGEND DESKTOP & LEGEND MOBILE

//--LEGEND MOBILE
function hideLegendMobile () {
    legendSideBarMobile.style.display = "none"; 
}

$(document).ready(function() {
	$( ".btn-primary-legend" ).click(function() {
	  $( "#legend-mobile" ).slideToggle( "slow" );
	})
});

hideLegendMobile(); 


//--LEGEND DESKTOP

function showLegend () {
    legendSideBar.style.display = "block"; 
}
function hideLegend () {
    legendSideBar.style.display = "none"; 
}

hideLegend ();
function toggleLegend () {
    if(legendSideBar.style.display == "block") { 
        hideLegend();
       
    } else if  (legendSideBar.style.display == 'none') {
        showLegend();
    }
}

buttonLegend.addEventListener('click', toggleLegend);


// ----FORM
// -FORM TOGGLE SLIDE

function toggleForm() { 
    if(formSideBar.style.display == "block") { 
        hideForm(); 
    } else if(formSideBar.style.display == 'none') {
        showForm(); 
    }
}

function showForm() {
     formSideBar.style.display = "block";    
} 
function hideForm() {
    formSideBar.style.display = "none"; 
}

buttonClose.addEventListener('click', hideForm);

// --FORM TOGGLE DOWN

function hideFormDown() {
	formDown.style.display = "none"
}

hideFormDown();

$(document).ready(function(){
	});


// ----MAPS
function getLatLong (stations) { // On souhaite récupérer les infos par station qui viennent de JC DECaux

		let stationClosed = {address: "RUE LEON SALVA", available_bike_stands: 15, available_bikes: 5, banking: true, bike_stands: 20, contract_name: "rouen", last_update: 1555520735000, name: "106-RUE LEON SALVA", number: 12, position: {lat: 49.4164295, lng: 1.086761}, lat:49.4164295, lng: 1.086761, status: "WORKS"};
		let stationWorks = {address: "RUE BENOIT MALON", available_bike_stands: 7, available_bikes: 13, banking: true, bike_stands: 20, contract_name: "rouen", last_update: 1555520735000, name: "106-RUE LEON SALVA", number:12, position: {lat: 49.4171966, lng: 1.088580},lat: 49.417196,lng: 1.088580, status: "CLOSED"};
		stations.push(stationClosed,stationWorks);

    stations.forEach(function (station) {

      let icon = null;
      switch(station.status) {
        case 'OPEN':
          icon = standardIcon;
          break;
        case 'CLOSED':
          icon = closedIcon;
          break;
        case 'WORKS':
          icon = worksIcon;
          break;
      }

      let pointer = L.marker([station.position.lat,station.position.lng,station.address],{icon: icon});
      const stationObject = new Station(station.address, station.available_bike_stands, station.available_bikes)
      
      markers.addLayer(pointer);
      mymap.addLayer(markers);
 
      let popUpContent = "<div id='station-summary'>";
      popUpContent += "<b>"+ stationObject.address+"</b>";
      popUpContent += "<br><br>";
      popUpContent += "Nombre de places disponibles : <span class='bikeStands'>" + stationObject.bikeStands+"</span><br>";
      popUpContent += "Nombre de vélos disponibles : <span class='bikes'>" + stationObject.bikes+"</span><br>";
      popUpContent += "</div>";
      popUpContent += "<br>";
      popUpContent += '<button data-station-address="'+ stationObject.address +'" class="bookButton"> Réserver</i></button><br>';
      

      let leafletPopUp = L.popup().setContent(popUpContent); 
      pointer.bindPopup(popUpContent,pointer)//Variable locale

    });
}


ajaxGet("https://api.jcdecaux.com/vls/v1/stations?contract=Rouen&apiKey=d71ded405c3aa486cde3a898f83ad0801bc8a329", getLatLong);

mymap.on('popupopen', (event) => {
    // récpérer le contenu de la popup 
    // filtrer et récupérer que div.popupInfo
    sourceElement = event.popup.getElement(); // on recupere l'élément html de la popup pour faire une recherche du bouton dedans
    let buttonForm = sourceElement.getElementsByClassName("bookButton");
    buttonForm = buttonForm[0];// affiche le 1er element qui a la classe
    hideForm(); // on force la dissimulation de la form au démarrage

    let addressInSession = sessionStorage.getItem('station');
    let popupAddress = buttonForm.dataset.stationAddress;
    console.log(popupAddress);
    if(addressInSession == popupAddress) {
      let bikeStandsElement = sourceElement.getElementsByClassName('bikeStands')[0];
      let bikesElement = sourceElement.getElementsByClassName('bikes')[0];
      let bikeStands = parseInt(bikeStandsElement.innerHTML) + 1;
      let bikes = parseInt(bikesElement.innerHTML) - 1;
      bikeStandsElement.innerHTML = bikeStands;
      bikesElement.innerHTML = bikes;
    }

    buttonForm.addEventListener('click', function(e) {
        selectedStation = e.target.dataset.stationAddress;
    		let stationSummary = document.getElementById('station-summary');
        toggleForm();
        let stationInfo = document.getElementById('station-info');
        stationInfo.innerHTML = stationSummary.innerHTML;
         // au clic sur button form, fonction anonyme qui remplace #station-summary par ce contenu et appelle toggleForm pour l'affiche
    }); 
});


$(document).ready(function() {
  $(".bookButton").click(function() { // Gestion du Scroll
  $([document.documentElement, document.body]).animate({
  scrollTop: $("#form").offset().top
     }, 2000);
  });
});

 
//FORM
let buttonSubmit = document.getElementById("button-submit");
let clock = document.getElementById('clock');
let limitTime = 1200000;
let stationInfo = document.getElementById('station-info');
let stationBooked = document.getElementById('station-booked');
let stationSummary = document.querySelector('#station-summary');
let bookingSubmitted = document.getElementById('bookingSubmitted');

  

function onSubmit (e) { 
	e.preventDefault(); //cet evenement je le recois mais j ne veux pas qu'il fasse ce qu'il fait d'hab
	let getStorageTime = sessionStorage.getItem('startTime');
  if (getStorageTime !== null) {
    clearBooking();
  }

  let name = document.getElementById('name').value; 
	let familyName = document.getElementById('family-name').value;
  let fromStation = selectedStation;
	let c = new Customer(name, familyName); // je crée un nouveau customer
	let b = new Booking(c, fromStation); //  je fais le lien avec c ou j'i sotcké mon customer que j'englobe dans mon booking	
  // b.countDown.tickCallback = function(obj) {
  //   let currentBookingElement = document.getElementById('currentBooking');
  //   currentBookingElement.getElementsByClassName('countdown')[0].innerHTML = obj.stringDelta;
  // };
  // b.countDown.endCallback = function(obj) {
  //   console.log('end');
  //   b.end();
  // };
  b.start();
  //updateCurrentBooking();
  alert('Votre réservation a bien été prise en compte !');
	
	// je fais demarrer mon booking
}

form.addEventListener('submit', onSubmit);

// Updates the status box every 500ms
function statusUpdate() {
  
  setInterval(function() {
  
    let currentBookingElement = document.getElementById('currentBooking');
    let endTime = sessionStorage.getItem('endTime');
    let station = sessionStorage.getItem('station');
    
    if(endTime && station) { //ET logique (&&)

      currentBookingElement.style.display = 'block';

      let now = new Date().getTime(); // Get todays date and time
      let delta = endTime - now;
      let minutes = Math.floor((delta % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((delta % (1000 * 60)) / 1000);
      let stringDelta = minutes + "m " + seconds + "s";

      if(delta <= 0) {
        alert("Votre réservation de 20 minutes a expiré.");
        clearEverything();
      }

      currentBookingElement.getElementsByClassName('address')[0].innerHTML = station;
      currentBookingElement.getElementsByClassName('countdown')[0].innerHTML = stringDelta;
    }
    else {
      currentBookingElement.style.display = 'none';
    }

  }, 500);

} statusUpdate();


// function showStationSubmission () {
//     bookingSubmitted.style.display = "block"; 
// }

// function hideStationSubmission () {
//     bookingSubmitted.style.display = "none"; 
// }

// hideStationSubmission ();


// function toggleStationSubmission () {
//     if( bookingSubmitted.style.display == "block") { 
//         hideStationSubmission();
       
//     } else if  (bookingSubmitted.style.display == 'none') {
//        showStationSubmission();
//     }
// }



$(document).ready(function(){
    let $familyName = $('#family-name'),
        $name = $('#name'),
        $submit = $('#button-submit'),
        $input = $('input'),
        $error = $('#error'),
        $reset = $('#refresh');

    $("#error").hide(); // on cache la div
    $input.keyup(function(){ 
        if($(this).val().length < 2){ //proprieté length
            $(this).css({
                borderColor: 'red', // on change le champs en rouge si champs > 1 caractère
                color: 'red'
            });//console.log('test');
        }
        else {
            $(this).css({
                borderColor : 'green', //on laisse le champs en vert
                color : 'green' // fond du champs en vert
            });
        }
    });

    $reset.click(function(){
        $input.css({ // on remet le style des champs comme on l'avait défini dans le style CSS
            borderColor : '#ccc',
            color : '#555'
        });
        $error.css('display', 'none'); // on prend soin de cacher le message d'erreur
    });

    function verifier(input){
    if(input.val() == ""){ // si le champ est vide
        $error.css('display', 'block'); // on affiche le message d'erreur
        input.css({ // on rend le champ rouge
            borderColor : 'red',
            color : 'red'
        }); 
       }
    }
});


//CANVAS

let can = $('#signature')[0],
        ctx = can.getContext('2d'), // permet d'accéder au contexte du canvas
        mousePressed = false,
        mouseX = 0,
        mouseY = 0;

can.addEventListener('touchmove', onTouchMove, false);
can.addEventListener('touchstart', onTouchStart, false);
can.addEventListener('touchend', onMouseUp, false);


// Tablet
function onTouchMove(event){
    if (mousePressed) {
        event.preventDefault();
        mouseX = (event.targetTouches[0].clientX) - canPos.x;
        mouseY = (event.targetTouches[0].clientY) - canPos.y;
        ctx.lineTo(mouseX, mouseY); //ajoute un segment
        ctx.stroke();
        //console.log(counterPixels);
				if (counterPixels >= 5) { 
					emptySignature = false;
					document.getElementById("button-submit").disabled = false;
					//console.log("il faut signer");
				} 
        document.getElementById("button-submit").disabled = false;
    }
}

function onTouchStart(event){
    mousePressed = true;
    canPos = can.getBoundingClientRect();
    mouseX = (event.targetTouches[0].clientX) - canPos.x;
    mouseY = (event.targetTouches[0].clientY) - canPos.y;
    ctx.beginPath();
    ctx.moveTo(mouseX, mouseY); 
    emptySignature = false;
    // document.getElementById("button-submit").disabled = false; 
}

function onMouseUp(event){
    mousePressed = false;
}

// Desktop
can.addEventListener('mousemove', onMouseMove, false);
can.addEventListener('mousedown', onMouseDown, false);
can.addEventListener('mouseup', onMouseUp, false);

function onMouseMove(event) {
    //console.log('mousemove');
    if (mousePressed) {
        event.preventDefault();
        canPos = can.getBoundingClientRect()
        mouseX = event.clientX - canPos.x;
        mouseY = event.clientY - canPos.y;
        ctx.lineTo(mouseX, mouseY);
        ctx.stroke();
        counterPixels++; 
        //console.log(counterPixels);	
				if (counterPixels >= 5) { 
					emptySignature = false;
					document.getElementById("button-submit").disabled = false;
					//console.log("il faut signer");
				} 

        emptySignature = false;
        document.getElementById("button-submit").disabled = false;
    }
}

function onMouseDown(event) {
    //console.log('mousedown');
    mousePressed = true;
    canPos = can.getBoundingClientRect()// renvoie a la taille de l'élément et de sa position relative par rapport à la zone d'affichage
    mouseX = event.clientX - canPos.x;
    mouseY = event.clientY - canPos.y;
    ctx.beginPath();// commence nouveau chemin
    ctx.moveTo(mouseX, mouseY); //déplacer le stylo à une position de départ pour dessiner une ligne.
    emptySignature = false;
   
}

$('#clearsig')[0].addEventListener('click', clearSignature, false);

function clearSignature() {
    ctx.clearRect(0, 0, can.width, can.height);
    document.getElementById("button-submit").disabled = true;
}


 





